;;; -*- no-byte-compile: t -*-
(define-package "ivy-youtube" "20170822.659" "Query YouTube and play videos in your browser" '((request "0.2.0") (ivy "0.8.0") (cl-lib "0.5")) :commit "9a9d584124bd9905b5a8a7e29cdafa3224770c9d" :url "https://github.com/squiter/ivy-youtube" :keywords '("youtube" "multimedia" "mpv" "vlc"))
