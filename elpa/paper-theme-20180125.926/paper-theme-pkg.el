;;; -*- no-byte-compile: t -*-
(define-package "paper-theme" "20180125.926" "A minimal Emacs colour theme." '((emacs "24")) :commit "cb4f920c85e04c5542f6ebccad189bb02ae75d2a" :url "http://gkayaalp.com/emacs.html#paper" :keywords '("theme" "paper"))
