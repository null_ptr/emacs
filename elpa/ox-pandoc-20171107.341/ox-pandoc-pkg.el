;;; -*- no-byte-compile: t -*-
(define-package "ox-pandoc" "20171107.341" "org exporter for pandoc." '((org "8.2") (emacs "24") (dash "2.8") (ht "2.0") (cl-lib "0.5")) :commit "55861adfceeae436deeae8402f545b771ad3484b" :url "https://github.com/kawabata/ox-pandoc" :keywords '("tools"))
