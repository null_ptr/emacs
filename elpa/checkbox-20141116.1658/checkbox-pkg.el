;;; -*- no-byte-compile: t -*-
(define-package "checkbox" "20141116.1658" "Quick manipulation of textual checkboxes" '((emacs "24") (cl-lib "0.5")) :commit "335afa4404adf72973195a580458927004664d98" :url "http://github.com/camdez/checkbox.el" :keywords '("convenience"))
