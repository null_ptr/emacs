;;; -*- no-byte-compile: t -*-
(define-package "runtests" "20150807.131" "Run unit tests from Emacs" 'nil :url "https://github.com/sunesimonsen/emacs-runtests" :keywords '("test"))
