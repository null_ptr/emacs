;;; -*- no-byte-compile: t -*-
(define-package "minor-mode-hack" "20170925.1734" "Change priority of minor-mode keymaps" 'nil :commit "9688994e23ccb2de568225ef125b41c46e5667c3" :url "http://www.emacswiki.org/cgi-bin/wiki/download/minor-mode-hack.el" :keywords '("lisp"))
