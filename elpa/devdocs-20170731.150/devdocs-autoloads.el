;;; devdocs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "devdocs" "devdocs.el" (23178 37614 629370
;;;;;;  638000))
;;; Generated autoloads from devdocs.el

(autoload 'devdocs-search "devdocs" "\
Launch Devdocs search.
CONFIRM goes with asking for confirmation.

\(fn &optional CONFIRM)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; devdocs-autoloads.el ends here
