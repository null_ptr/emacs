;;; -*- no-byte-compile: t -*-
(define-package "tango-plus-theme" "20161016.322" "A color theme based on the tango palette" 'nil :url "https://github.com/tmalsburg/tango-plus-theme")
