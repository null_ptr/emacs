;;; imgur-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (imgur-post imgur-post-image) "imgur" "imgur.el"
;;;;;;  (22413 4266 744529 98000))
;;; Generated autoloads from imgur.el

(autoload 'imgur-post-image "imgur" "\


\(fn &optional FILENAME)" nil nil)

(autoload 'imgur-post "imgur" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("imgur-pkg.el") (22413 4266 905916 729000))

;;;***

(provide 'imgur-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; imgur-autoloads.el ends here
