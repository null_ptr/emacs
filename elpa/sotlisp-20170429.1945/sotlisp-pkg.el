;;; -*- no-byte-compile: t -*-
(define-package "sotlisp" "20170429.1945" "Write lisp at the speed of thought." '((emacs "24.1")) :commit "89dfed2b5d2e9a3b16bfc47f169412b583626059" :url "https://github.com/Malabarba/speed-of-thought-lisp" :keywords '("convenience" "lisp"))
