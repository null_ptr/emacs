;;; ac-emoji-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "ac-emoji" "ac-emoji.el" (23141 43194 294253
;;;;;;  52000))
;;; Generated autoloads from ac-emoji.el

(autoload 'ac-emoji-setup "ac-emoji" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("ac-emoji-data.el" "ac-emoji-pkg.el")
;;;;;;  (23141 43194 261252 715000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ac-emoji-autoloads.el ends here
