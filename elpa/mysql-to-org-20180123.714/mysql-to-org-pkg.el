;;; -*- no-byte-compile: t -*-
(define-package "mysql-to-org" "20180123.714" "Minor mode to output the results of mysql queries to org tables" '((emacs "24.3") (s "1.11.0")) :commit "2526205ad484ad3fa38d41e7d537ace38c27645c")
