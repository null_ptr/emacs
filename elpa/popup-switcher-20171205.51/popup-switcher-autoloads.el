;;; popup-switcher-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "popup-switcher" "popup-switcher.el" (23085
;;;;;;  5902 464496 425000))
;;; Generated autoloads from popup-switcher.el

(autoload 'psw-switch-buffer "popup-switcher" "\


\(fn)" t nil)

(autoload 'psw-switch-recentf "popup-switcher" "\


\(fn)" t nil)

(autoload 'psw-switch-projectile-files "popup-switcher" "\


\(fn)" t nil)

(autoload 'psw-navigate-files "popup-switcher" "\


\(fn &optional START-PATH)" t nil)

(autoload 'psw-switch-function "popup-switcher" "\


\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; popup-switcher-autoloads.el ends here
