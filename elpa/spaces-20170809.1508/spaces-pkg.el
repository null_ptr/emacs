;;; -*- no-byte-compile: t -*-
(define-package "spaces" "20170809.1508" "Create and switch between named window configurations." 'nil :commit "6bdb51e9a346907d60a9625f6180bddd06be6674" :url "https://github.com/chumpage/chumpy-windows" :keywords '("frames" "convenience"))
