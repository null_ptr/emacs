;;; -*- no-byte-compile: t -*-
(define-package "lang-refactor-perl" "20131122.1327" "Simple refactorings, primarily for Perl" 'nil :commit "691bd69639de6b7af357e3b7143563ececd9c497" :url "https://github.com/jplindstrom/emacs-lang-refactor-perl" :keywords '("languages" "refactoring" "perl"))
