;;; -*- no-byte-compile: t -*-
(define-package "langtool" "20180409.316" "Grammar check utility using LanguageTool" '((cl-lib "0.3")) :commit "d93286722cff3fecf8641a4a6c3b0691f30362fe" :keywords '("docs") :url "https://github.com/mhayashi1120/Emacs-langtool")
